## Requirements

- NodeJS ^4
- MongoDB

## Install the dependencies

>`$ npm install`

## Run the server

>`$ node app`
