'use strict';

const mongodb = require('mongodb')
const Db = mongodb.Db;
const Server = mongodb.Server;
const ObjectID = mongodb.ObjectID;
const express = require('express');
const bodyParser= require('body-parser')
const app = express();

const seed = require('./seed.js');

app.use(bodyParser.json())


// CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods',' GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});


// Auth
let isAuthenticated = (req, res, next) => {
  let allowedTokens = ['Bearer 800813555'];
  let authToken = req.headers['authorization'];

  if(!authToken){
    return res.status(403).send({ message: 'No Authorization header present' }).end();
  }

  if(allowedTokens.indexOf(authToken) === -1) {
    return res.status(401).send({ message: 'Invalid token' }).end();
  }

  next();

};


// API Responses
let respond = (res, status, data) => {
  if(!data){
    return res.status(status).end();
  }
  return res.status(status).send(data).end();
}

// Database methods

let insertRecord = (record, res) => {
  return db.collection('things').save(record, (err, result) => {
    if (err) {
      if(res){
        return respond(res, 500, {message: 'Ooops!'});
      }
      console.log('Error creating data');
    }
    if(!res){
      console.log('Data created!');
      return;
    }
    return respond(res, 201, result.ops);
  });
}


// DB stuff

let db = new Db('testApp', new Server('localhost', 27017));
// Open DB connection
db.open((err, db) => {

  // Create a collection
  db.createCollection('things', (err, result) => {
    if(!err){
      if(seed.data.length){
        db.collection('things').deleteMany( {}, (err, results) => {
          seed.data.forEach(dummyObj =>{
            insertRecord(dummyObj);
          });
          console.log('Seed restored');
        });
      }
    }
  });

});


// API

app.get('/api/things', (req, res) => {
  db.collection('things').find().toArray((err, results) => {
    return respond(res, 200, results);
  });
});

app.get('/api/things/:id', (req, res) => {
  db.collection('things').findOne({},{},{"_id": req.params.id}, (err, result) => {
    return respond(res, 200, result);
  });
});

app.post('/api/things', (req, res) => {
  insertRecord(req.body, res);
});

app.put('/api/things/:id', (req, res) => {
  if(req.body._id) {
    delete req.body._id;
  }
console.log('PUT request')
  db.collection('things').update({"_id": new ObjectID(req.params.id)}, req.body, (err, result) => {
    if(err){
      return respond(res, 500, 'Ooops!');
    }
    return respond(res, 200);
  });
});

app.delete('/api/things/:id', (req, res) => {
  db.collection('things').deleteOne({"_id": new ObjectID(req.params.id)}, (err, result) => {
    if (err) {
      return respond(res, 500);
    } else {
      return respond(res, 204);
    }
  });
});  



app.listen(3000, _ => console.log('API running on port 3000'));