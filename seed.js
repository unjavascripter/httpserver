module.exports = this;

this.data = [
  {
    name: 'TypeScript - Introducción y práctica',
    date: {
      start: 1468256400000,
      end: 1468270800000
    },
    description: 'Laboris cupidatat laboris deserunt nostrud duis pariatur id eu voluptate.',
    speaker: {
      name: 'John Doe',
      twitter: 'js'
    }
  },
  {
    name: 'Angular 2 - Introducción, estructura y práctica',
    date: {
      start: 1468342800000,
      end: 1468357200000
    },
    description: 'Ea non aliqua proident esse commodo ex labore nulla qui nulla aliquip aliquip esse. Dolor consectetur ut id irure.',
    speaker: {
      name: 'Diego Coy',
      twitter: 'unJavaScripter'
    }
  },
  {
    name: 'Angular 2 - RXJS',
    date: {
      start: 1469039400000,
      end: 1469041200000
    },
    description: 'Excepteur quis excepteur ad officia esse mollit voluptate sunt commodo quis culpa ut. Dolore nostrud dolore tempor aliqua ex sit magna. Irure nisi elit officia incididunt nisi sunt quis.',
    speaker: {
      name: 'Simón Bolivar',
      twitter: 'arrayPuntoJoin'
    }
  },
  {
    name: 'Web Bluetooth - OMG!!!',
    description: 'Enim exercitation nostrud anim aliqua voluptate et nisi ea exercitation laborum eu sit.',
    speaker: {
      name: 'Diego Coy',
      twitter: 'unJavaScripter'
    }
  },
  {
    name: 'Progressinve Web Apps - El futuro (¡¡¡presente!!!) de la web',
    date: {
      start: 1468582200000,
      end: 1468585800000
    },
    description: 'Quis ex esse consequat nisi ex dolore anim labore nisi. Aliqua laborum sit ea et eu incididunt id in ea incidi reprehenderit amet sunt cillum magna fugiat.',
    speaker: {
      name: 'Diego Coy',
      twitter: 'unJavaScripter'
    }
  },
]